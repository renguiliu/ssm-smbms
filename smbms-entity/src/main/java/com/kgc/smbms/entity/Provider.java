package com.kgc.smbms.entity;

import lombok.Data;

@Data
public class Provider{

    private Integer id;
    private String name;
}