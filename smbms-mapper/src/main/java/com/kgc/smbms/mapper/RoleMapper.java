package com.kgc.smbms.mapper;

import com.kgc.smbms.entity.Role;

import java.util.List;

/**
 * 用户管理模块的数据访问层接口
 *
 * @author lrg on 2021/6/8
 * @since 1.0.0
 */
public interface RoleMapper {

    List<Role> queryAll();
}
