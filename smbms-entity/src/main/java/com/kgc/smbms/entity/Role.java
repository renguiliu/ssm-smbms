package com.kgc.smbms.entity;

import lombok.Data;
import lombok.ToString;

import java.util.Date;
import java.util.List;

@Data
@ToString
public class Role {

    private Integer id;
    private String roleCode;
    private String roleName;
    private Integer createdBy;   //创建者
    private Date creationDate; //创建时间
    private Integer modifyBy;     //更新者
    private Date modifyDate;   //更新时间

    private List<User> userList;//一个角色对应多个用户
}
