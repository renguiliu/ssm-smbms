package com.kgc.smbms.controller;

import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;

import java.beans.PropertyEditor;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 基本的控制器
 * @author lrg on 2021/7/6
 * @since 1.0.0
 */
public class BaseController {

    /**
     * 使用@initDataBinder解决SpringMVC日期类型无法转换的问题
     * @param dataBinder
     */
    @InitBinder
    public void initBinder(WebDataBinder dataBinder){
        PropertyEditor propertyEditor
                = new CustomDateEditor(new SimpleDateFormat("yyyy-MM-dd"),true);
        //注册自定义编辑器
        dataBinder.registerCustomEditor(Date.class,propertyEditor);
    }
}
