package com.kgc.smbms.controller;

import com.kgc.smbms.entity.User;
import com.kgc.smbms.service.UserService;
import com.kgc.smbms.utils.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpSession;

/**
 * 登录的控制器
 *
 * @author lrg on 2021/6/28
 * @since 1.0.0
 */
@Controller
public class LoginController {

    @Autowired
    private UserService userService;

    /**
     * 显示登录页面
     * @return
     */
    /*@RequestMapping(value="/loginview")
    public String loginView(){
        return "login";
    }*/

    @RequestMapping(value="/login",method = RequestMethod.POST)
    public String login(String loginParams,
                        String password,
                        Model model,
                        HttpSession session){
        User user = userService.login(loginParams,password);
        if(user!=null){//登录成功
            //1.将用户的信息存储到HttpSession中
            session.setAttribute(Constants.LOGIN_USER,user);
            //2.重定向到欢迎页面
            return "redirect:welcome";//不能重定向到页面，只能重定向到控制器
            //return "redirect:user/welcome";// 前面没有/表示在当前控制器中跳转
        }
        //如果代码能走到这里，说明登录失败
        //1.保存错误信息
        model.addAttribute("loginMsg","用户名或密码错误");
        //2.跳转到登录页面
        return "login"; //转发
    }

    /**
     * 跳转到欢迎页面
     * @return
     */
    @RequestMapping(value = "/welcome")
    public String welcome(){
        return "welcome";
    }
}
