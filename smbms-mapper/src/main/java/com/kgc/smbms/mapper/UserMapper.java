package com.kgc.smbms.mapper;

import com.kgc.smbms.entity.User;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 用户管理模块的数据访问层接口
 *
 * @author lrg on 2021/6/8
 * @since 1.0.0
 */
public interface UserMapper {

    List<User> getUserByCondition(Map<String, Object> params);

    Integer create(User user);

    Integer updateByPrimaryKey(User user);

    Integer updatePassByPrimaryKey(@Param("id") Integer id,
                                   @Param("newPassword") String newPassword);

    Integer deleteByPrimaryKey(Integer id);

    User getUserByPrimaryKey(Integer id);

    Integer validateData(Map<String, Object> params);

    List<User> login(@Param("loginParams") String loginParams);
}
