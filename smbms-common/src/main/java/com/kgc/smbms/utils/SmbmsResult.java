package com.kgc.smbms.utils;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 前后端交互的工具类
 *
 * @author lrg on 2021/6/15
 * @since 1.0.0
 */
@Data
@NoArgsConstructor
public class SmbmsResult {

    /**http的状态码*/
    private Integer code;
    /**响应到前端的信息*/
    private String message;
    /**响应到前端的数据*/
    private Object data;

    public SmbmsResult(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

    public SmbmsResult(Integer code, String message, Object data) {
        this.code = code;
        this.message = message;
        this.data = data;
    }

    public SmbmsResult(String message) {
        this.message = message;
    }
}
