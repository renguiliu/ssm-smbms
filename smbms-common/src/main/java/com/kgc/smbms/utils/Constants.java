package com.kgc.smbms.utils;

/**
 * 常量类
 *
 * @author lrg on 2021/6/11
 * @since 1.0.0
 */
public class Constants {

    public static final String LOGIN_USER = "LOGIN_USER";

    /**
     * 分页的常量类
     */
    public static class PageUtils{
        /*当前页码*/
        public static final Integer currentIndex = 1;
        /*每页显示5条数据*/
        public static final Integer pageSize = 5;
    }
}
