<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="fm"%>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<html>
<head lang="en">
    <base href="<%=basePath%>">
    <meta charset="UTF-8">
    <title>超市账单管理系统</title>
    <link rel="stylesheet" href="css/public.css"/>
    <link rel="stylesheet" href="css/style.css"/>
    <link rel="stylesheet" href="bootstrap/css/bootstrap.css">
</head>
<body>
<!--头部-->
<jsp:include page="../common/header.jsp"/>
<!--主体内容-->
<section class="publicMian">
    <jsp:include page="../common/left.jsp"/>
    <div class="right">
        <div class="location">
            <strong>你现在所在的位置是:</strong>
            <span>用户管理页面 >> 用户添加页面</span>
        </div>
        <div class="providerAdd">
            <form action="user/add" method="post" id="addForm" enctype="multipart/form-data">
                <!--div的class 为error是验证错误，ok是验证成功-->
                <div class="">
                    <label for="userCode">用户编码：</label>
                    <input type="text" name="userCode" id="userCode"/>
                    <span>*请输入用户编码，且不能重复</span>
                    <span>${userCode}</span>
                </div>
                <div>
                    <label for="userName">用户名称：</label>
                    <input type="text" name="userName" id="userName"/>
                    <span >*请输入用户名称</span>
                    <span>${userName}</span>
                </div>
                <div>
                    <label for="userpassword">用户密码：</label>
                    <input type="text" name="userPassword" id="userpassword"/>
                    <span>*密码长度必须大于6位小于20位</span>

                </div>
                <div>
                    <label for="userRemi">确认密码：</label>
                    <input type="text" name="userRemi" id="userRemi"/>
                    <span>*请输入确认密码</span>
                </div>
                <div>
                    <label >用户性别：</label>

                    <select name="gender">
                        <option value="1">男</option>
                        <option value="2">女</option>
                    </select>
                    <span></span>
                </div>
                <div>
                    <label for="data">出生日期：</label>
                    <%--<input type="text" name="birthday" class="text Wdate" onclick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss'});" id="data"/>--%>
                    <input type="text" name="birthday" class="text Wdate" id="data"/>
                    <span >*</span>
                </div>
                <div>
                    <label for="userphone">用户电话：</label>
                    <input type="text" name="phone" id="userphone"/>
                    <span >*</span>
                </div>
                <div>
                    <label for="userAddress">用户地址：</label>
                    <input type="text" name="address" id="userAddress"/>
                </div>
                <div id="role">
                    <label for="userRole">用户角色：</label>
                    <c:forEach items="${roleList}" var="role">
                        <input type="radio" name="userRole" value="${role.id}"/>${role.roleName}
                    </c:forEach>
                </div>
                <div>
                    <label for="picPath" style="float: left;" >头像：</label>
                    <input id="oldHeaderPicPath" type="hidden" name="oldPicPath"/>
                    <%--这个值是提交到后台user对象的属性picPath上的值--%>
                    <input id="headPicPath" type="hidden" name="picPath"/>
                    <input type="file" name="headerPic" id="headerPic"/>
                    <span id="errMsg_1" style="color: red;"></span>
                    <div class="yl">
                        <label for="header_yl">预览：</label>
                        <img id="header_yl" src="img/moren.jpg" width="150px" height="150px">
                    </div>
                </div>

                <div>
                    <label for="workPicPath" style="float: left;" >证件照：</label>
                    <input id="oldWorkPicPath" type="hidden" name="oldWorkPicPath"/>
                    <%--这个值是提交到后台user对象的属性workPicPath上的值--%>
                    <input id="workPicPath" type="hidden" name="workPicPath"/>
                    <input type="file" name="headerPic" id="workPic"/>
                    <span id="errMsg_2" style="color: red;"></span>
                    <div class="yl">
                        <label for="work_yl">预览：</label>
                        <img id="work_yl" src="img/moren.jpg" width="150px" height="150px">
                    </div>
                </div>

                <div class="providerAddBtn">
                    <!--<a href="#">保存</a>-->
                    <!--<a href="userList.html">返回</a>-->
                    <input type="button" value="确认上传" id="uploadBtn"/>
                    <input type="button" value="保存" id="addBtn"/>
                    <input type="button" value="返回" onclick="history.back(-1)"/>
                </div>
            </form>
        </div>

    </div>
</section>
<footer class="footer">
    版权归北大青鸟
</footer>
<script src="js/time.js"></script>
<script type="text/javascript" src="js/jquery-1.12.4.js"></script>
<script type="text/javascript" src="js/ajaxfileupload.js"></script>
<script type="text/javascript" src="My97DatePicker/WdatePicker.js"></script>
<script type="text/javascript">
    $(document).ready(function(){

       /* $("#uploadBtn").click(function(){
            let fileEleId = ["headerPic","workPic"];
            uploadFile(fileEleId,,"manyupload");
        });*/

        /*
        * 当文件域中的值发生改变的时候触发时间
        */
        $("#headerPic").on('change',function(){
            let oldPicPath = $("#oldHeaderPicPath").val();
            if((oldPicPath!=null && oldPicPath!="")){
                $.post("delete",{"imgPath":oldPicPath},function(data){
                    if(data.flag){
                        uploadFile("headerPic","upload");
                    }else{
                        alert("服务器错误!");
                    }
                },"json");
                return ;
            }
            uploadFile("headerPic","upload");
        });

        /*
        * 当文件域中的值发生改变的时候触发时间
        */
        $("#workPic").on('change',function(){
            let oldPicPath = $("#oldWorkPicPath").val();
            if((oldPicPath!=null && oldPicPath!="")){
                $.post("delete",{"imgPath":oldPicPath},function(data){
                    if(data.flag){
                        uploadFile("workPic","upload");
                    }else{
                        alert("服务器错误!");
                    }
                },"json");
                return ;
            }
            uploadFile("workPic","upload");
        });

        /*
        文件上传的方法
        fileEleId:文件域的ID  [可以是一个数组]
        */
        function uploadFile(fileEleId,url){
            //开始文件上传
            $.ajaxFileUpload({
                url: url, //用于文件上传的服务器端请求地址
                type: 'post',
                secureuri: false, //是否需要安全协议，一般设置为false
                fileElementId: fileEleId, //文件上传域的ID
                dataType: 'json', //返回值类型 一般设置为json
                async: true,      //将异步请求(false默认)设置为同步请求(true)
                success: function (data, status){//服务器成功响应处理函数
                    //如果现在是多文件上传，说明fileEleId是一个数组
                    if (fileEleId instanceof Array){
                        $.each(data,function(index,result){
                            updateEle(fileEleId[index],result);
                        });
                    }else{//单文件上传
                        updateEle(fileEleId,data[0]);
                    }
                },
                error:function(data, status, e){ //服务器响应失败时的处理函数
                    $('#result').html('图片上传失败，请重试！！');
                }
            })
        }

        /*
        * 文件上传之后，修改相关元素节点的属性值
        * [
        *   {"code":200,"data":"upload\\1625201003610234.jpg","message":"文件上传成功"},
        *   {"code":500,"data":"","message":"上传的文件格式不正确，只能是jpg,png,bmp"}
        * ]
        */
        function updateEle(fileEleId,data){
            if(data.code==500){
                $("#"+fileEleId).next().text(data.message);//设置错误的提示信息在span标签中
                $("#"+fileEleId).parent().children(".yl").children("img:last-child").attr("src", "img/moren.jpg");
                //设置头像属性的地址
                $("#"+fileEleId).prev().val("");
                $("#"+fileEleId).prev().prev().val("");
                return ;
            }
            $("#"+fileEleId).next().text("");
            $("#"+fileEleId).parent().children(".yl").children("img:last-child").attr("src",data.data);
            /*picYl.attr("src", data.imgurl);*/
            //设置头像属性的地址
            $("#"+fileEleId).prev().val(data.data);
            $("#"+fileEleId).prev().prev().val(data.data);
        }



        $("#addBtn").on('click',function(){
            $("#addForm").submit();
        });

        //发送ajax请求获取角色的json数据
        /*$.getJSON("RoleServlet?method=allrolejson",function(result){
            var $role =  $("#role");
            //清空role的div
            $role.empty();
            $role.append("<label >用户类别：</label>");
            //将查询的结果拼接到div上
            $.each(result,function(index,role){
                $role.append("<input type='radio' name='userlei' value='"+role.id+"'/>"+role.roleName);
            });
        });*/

        //验证用户名和电话号码的唯一性
        //设置用户编码失去焦点事件
        $("#userCode").on('blur',function(){
            var userCodeVal = $(this).val();
            var messageSpan = $(this).next();//存放错误信息的span标签元素
            if(userCodeVal==null
                || userCodeVal==""
                || userCodeVal==undefined){
                messageSpan.css("color","red");
                messageSpan.html("请输入用户编码");
                return;
            }
            //发送ajax请求进行验证
            validateData(userCodeVal,1,messageSpan);
        });
        //设置电话号码失去焦点事件
        $("#userphone").on('blur',function(){
            var userphoneVal = $(this).val();
            var messageSpan = $(this).next();//存放错误信息的span标签元素
            if(userphoneVal==null
                || userphoneVal==""
                || userphoneVal==undefined){
                messageSpan.css("color","red");
                messageSpan.html("请输入电话码");
                return;
            }
            //发送ajax请求进行验证
            validateData(userphoneVal,2,messageSpan);
        });

        /**
         * 验证数据是否存在的方法
         * @param validataParam 需要验证的参数值
         * @param validataType  验证的类型：1.表示验证用户编码 2.表示验证电话号码
         * @param messageSpan  显示错误的信息的元素
         */
        function validateData(validataParam,validataType,messageSpan){
            var data = {"validataParam":validataParam,
                        "validataType":validataType};
            var url = "user/validata";
            //要求后台返回的数据为:
            // {"code":200/500,"message":"请求成功",data:flag}
            $.post(url,data,function(result){
               if(result.code==200){
                    if(result.data){//表示可用
                        messageSpan.css("color","green");
                        messageSpan.html("√");
                    }else{//表示不可用
                        messageSpan.css("color","red");
                        if(validataType==1){
                            messageSpan.html("该用户编码已被其他用户使用,请重新输入");
                        }
                        if(validataType==2){
                            messageSpan.html("该电话号码已被其他用户使用,请重新输入");
                        }
                    }
               }
            },"json");
        }
    });
</script>
</body>
</html>