package com.kgc.smbms.service.impl;

import com.kgc.smbms.entity.Role;
import com.kgc.smbms.mapper.RoleMapper;
import com.kgc.smbms.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author lrg on 2021/6/11
 * @since 1.0.0
 */
@Service("roleService")
public class RoleServiceImpl implements RoleService {

    @Autowired
    private RoleMapper roleMapper;

    @Override
    public List<Role> findAll() {
        List<Role> roleList = roleMapper.queryAll();
        return roleList;
    }
}
