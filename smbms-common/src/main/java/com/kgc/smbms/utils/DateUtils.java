package com.kgc.smbms.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * 日期的工具类
 *
 * @author lrg on 2021/6/15
 * @since 1.0.0
 */
public class DateUtils {

    public static Date strToDate(String dateStr,String pattern){
        Date date = null;
        try {
            SimpleDateFormat format = new SimpleDateFormat(pattern);
            date = format.parse(dateStr);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }

    /**
     * 出生日期转换成年龄
     * @param birthday
     * @return
     */
    public static Integer birthdayToAge(Date birthday) {
        //获取当前系统时间日历对象
        Calendar currentDate = Calendar.getInstance();
        //根据出生日期获取出生的时候的日历对象
        Calendar birthdayDate = Calendar.getInstance();
        birthdayDate.setTime(birthday);
        //获取当前时间和出生日期的时间年份的差值
        int age = currentDate.get(Calendar.YEAR) - birthdayDate.get(Calendar.YEAR);
        //当前时间的月份<出生日期的月份
        if(currentDate.get(Calendar.MONTH) < birthdayDate.get(Calendar.MONTH)){
            age = age -1;
        }
        //当前时间的月份==出生日期的月份
        if(currentDate.get(Calendar.MONTH) == birthdayDate.get(Calendar.MONTH)){
            //同月判断日期
            if(currentDate.get(Calendar.DAY_OF_MONTH) < birthdayDate.get(Calendar.DAY_OF_MONTH)){
                age = age -1;
            }
        }
        return age;
    }
}
