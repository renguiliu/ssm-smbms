package com.kgc.smbms.service;

import com.kgc.smbms.entity.Role;

import java.util.List;

/**
 * 角色模块的业务逻辑层
 *
 * @author lrg on 2021/6/11
 * @since 1.0.0
 */
public interface RoleService {

    List<Role> findAll();
}
