package com.kgc.smbms.service;

import com.github.pagehelper.PageInfo;
import com.kgc.smbms.entity.User;

import java.util.Map;

/**
 * 用户模块的业务逻辑层
 *
 * @author lrg on 2021/6/10
 * @since 1.0.0
 */
public interface UserService {

    PageInfo<User> getUserByCondition(Integer currentIndex,
                                      Integer pageSize,
                                      String username,
                                      Integer userRole);

    boolean validataData(Map<String, Object> params);

    boolean create(User user);

    User login(String loginParams, String password);

    boolean delete(int id);

    User getUserById(int id);

    boolean update(User user);
}
