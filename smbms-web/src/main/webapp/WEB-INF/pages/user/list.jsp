<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<html>
<head lang="en">
    <base href="<%=basePath%>">
    <meta charset="UTF-8">
    <title>超市账单管理系统</title>
    <link rel="stylesheet" href="css/public.css"/>
    <link rel="stylesheet" href="css/style.css"/>
    <link rel="stylesheet" href="bootstrap/css/bootstrap.css">
    <style>
        .clear:after{
            content: '';
            display: block;
            clear: both;
        }
    </style>
</head>
<body>
<jsp:include page="../common/header.jsp"/>
<!--主体内容-->
<c:if test="${not empty message}">
    <script>
        alert('${message}');
    </script>
</c:if>
<section class="publicMian">
    <jsp:include page="../common/left.jsp"/>
    <div class="right">
        <div class="location">
            <strong>你现在所在的位置是:</strong>
            <span>用户管理页面</span>
        </div>
        <form action="user/index" method="post" id="searchForm">
            <div class="search">
                <span>用户名：</span>
                <input type="hidden" name="currentIndex" value="1"/>
                <input type="text" name="username" placeholder="请输入用户名"
                       value="${requestScope.username}"/>
                <span>用户角色：</span>
                <select name="role">
                    <option value="0">--请选择--</option>
                    <c:forEach items="${requestScope.roleList}" var="role">
                        <option value="${role.id}" <c:if test="${role.id==requestScope.userRole}">selected</c:if>>
                            ${role.roleName}
                        </option>
                    </c:forEach>
                </select>
                <input type="button" id="searchBtn" value="查询"/>
                <a href="user/addview">添加用户</a>
            </div>
        </form>
        <!--用户-->
        <table class="providerTable" cellpadding="0" cellspacing="0">
            <tr class="firstTr">
                <th width="10%">用户编码</th>
                <th width="20%">用户名称</th>
                <th width="10%">性别</th>
                <th width="10%">年龄</th>
                <th width="10%">电话</th>
                <th width="10%">用户类型</th>
                <th width="30%">操作</th>
            </tr>
            <c:choose>
                <c:when test="${empty requestScope.page or empty requestScope.page.list or requestScope.page.list.size()==0}">
                    <tr>
                        <td colspan="7" align="center">没有相应的数据!</td>
                    </tr>
                </c:when>
                <c:otherwise>
                    <c:forEach items="${requestScope.page.list}" var="user" varStatus="in">
                        <tr>
                            <td>${user.userCode}</td>
                            <td>${user.userName}</td>
                            <c:choose>
                                <c:when test="${user.gender==1}"><td>男</td></c:when>
                                <c:when test="${user.gender==2}"><td>女</td></c:when>
                            </c:choose>
                            <td>${user.age}</td>
                            <td>${user.phone}</td>
                            <td>${user.role.roleName}</td>
                            <td>
                                <%--<a href="user/view/${user.id}">
                                    <img src="img/read.png" alt="查看" title="查看"/>
                                </a>--%>
                                    <a href="#" data-id="${user.id}" class="user-detail">
                                        <img src="img/read.png" alt="查看" title="查看"/>
                                    </a>
                                <a href="user/updateview/${user.id}">
                                    <img src="img/xiugai.png" alt="修改" title="修改"/>
                                </a>
                                <a href="#" class="removeUser" data-id="${user.id}" data-name="${user.userName}">
                                    <img src="img/schu.png" alt="删除" title="删除"/>
                                </a>
                            </td>
                        </tr>
                    </c:forEach>
                </c:otherwise>
            </c:choose>
        </table>
        <ul class="pagination" style="margin:10px 400px !important;">
            <li><a href="#" class="pageBtn" page-index="${requestScope.page.pageNum-1}">&laquo;</a></li>
            <c:forEach begin="1" end="${requestScope.page.pages}" var="index">
                <li <c:if test="${index==requestScope.page.pageNum}">class="active"</c:if>><a href="#" class="pageBtn" page-index="${index}">${index}</a></li>
            </c:forEach>
            <li><a href="#" class="pageBtn" page-index="${requestScope.page.pageNum+1}">&raquo;</a></li>
        </ul>
        <div class="user-info" style="display: none;">
            <table border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td>用户编码:</td>
                    <td><input type="text" readonly disabled name="code"/></td>
                </tr>
                <tr>
                    <td>用户姓名:</td>
                    <td><input type="text" readonly disabled name="name"/></td>
                </tr>
                <tr>
                    <td>用户性别:</td>
                    <td><input type="text" readonly disabled name="gender"/></td>
                </tr>
                <tr>
                    <td>出生日期:</td>
                    <td><input type="text" readonly disabled name="birthday"/></td>
                </tr>
                <tr>
                    <td>用户电话:</td>
                    <td><input type="text" readonly disabled name="telephone"/></td>
                </tr>
                <tr>
                    <td>用户角色:</td>
                    <td><input type="text" readonly disabled name="roleName"/></td>
                </tr>
                <tr>
                    <td>用户地址:</td>
                    <td><input type="text" readonly disabled name="address"/></td>
                </tr>
            </table>
        </div>
    </div>
</section>


<!--点击删除按钮后弹出的页面-->
<div class="zhezhao"></div>
<div class="remove" id="removeUse">
    <div class="removerChid">
        <h2>提示</h2>
        <div class="removeMain">
            <p>你确定要删除该用户吗？</p>
            <a href="#" id="yes">确定</a>
            <a href="#" id="no">取消</a>
        </div>
    </div>
</div>

<footer class="footer">
    版权归北大青鸟
</footer>

<script src="js/jquery.js"></script>
<script src="js/js.js"></script>
<script src="js/time.js"></script>
<script>
    $(document).ready(function(){

        /*点击查看用户详情的按钮*/
        $(".user-detail").on('click',function(e){
            e.preventDefault();
            //获取data-id的属性值
            let id = $(this).attr("data-id");
            $.post("user/detail",{"id":id},function(data){
                console.log(data);
                if(data.code==200){
                    $("input[name='code']").val(data.data.userCode);
                    $("input[name='name']").val(data.data.userName);
                    if(data.data.gender==1){
                        $("input[name='gender']").val("男");
                    }else{
                        $("input[name='gender']").val("女");
                    }
                    $("input[name='birthday']").val(data.data.birthday);
                    $("input[name='telephone']").val(data.data.phone);
                    $("input[name='roleName']").val(data.data.role.roleName);
                    $("input[name='address']").val(data.data.address);
                    $(".user-info").css("display","block");
                }
            },"json");
        });



        /*删除的JavaScript-------------开始*/
        var dataId;
        var dataName;
        //用户管理页面上点击删除按钮弹出删除框(userList.html)
        $('.removeUser').click(function (e) {
            e.preventDefault();
            dataId = $(this).attr("data-id");
            dataName = $(this).attr("data-name");
            //设置提示框中的内容
            $(".removeMain>p").html("您确定要删除【"+dataName+"】用户吗？");
            $('.zhezhao').css('display', 'block');
            $('#removeUse').fadeIn();
        });

        $('#yes').click(function (e) {
            e.preventDefault();
            $('.zhezhao').css('display', 'none');
            $('#removeUse').fadeOut();
            //发送请求到后台去做删除
            location.href="UserServlet?method=delete&id="+dataId;
        });

        $('#no').click(function (e) {
            e.preventDefault();
            $('.zhezhao').css('display', 'none');
            $('#removeUse').fadeOut();
        });
        /*删除的JavaScript-------------结束*/


        $("#searchBtn").on('click',function(){
            $("#searchForm").submit();//提交查询表单
        });

        $(".pageBtn").on('click',function(e){
            e.preventDefault();
            var pageIndex = $(this).attr("page-index");
            $("input[name='currentIndex']").val(pageIndex);
            $("#searchForm").submit();//提交查询表单
        });
    });
</script>
</body>
</html>