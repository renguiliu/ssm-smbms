package com.kgc.smbms.entity;

import com.alibaba.fastjson.annotation.JSONField;
import com.kgc.smbms.utils.DateUtils;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.validator.constraints.Length;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotNull;
import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class User {
	private Integer id; //id
	@NotNull(message="用户编码不允许为空")
	private String userCode; //用户编码
	@NotNull(message="用户姓名不允许为空")
	@Length(min = 5,max = 15,message = "用户姓名必须是5-15个字符之间")
	private String userName; //用户名称
	private String userPassword; //用户密码
	private Integer gender;  //性别
	@JSONField(format = "yyyy-MM-dd")
	/*@DateTimeFormat(pattern = "yyyy-MM-dd")*/
	private Date birthday;  //出生日期
	private String phone;   //电话
	private String address; //地址
	private Integer userRole;    //用户角色
	private Integer createdBy;   //创建者
	private Date creationDate; //创建时间
	private Integer modifyBy;     //更新者
	private Date modifyDate;   //更新时间
	private String picPath;    //头像照片存储地址
	private String workPicPath;//工作照片存储地址

	private Integer age;

	private String userRoleName;//该用户对应的角色名称

	private Role role;//一个用户对应一个角色

	public void setBirthday(Date birthday){
		this.birthday = birthday;
		this.age = DateUtils.birthdayToAge(birthday);
	}
}
