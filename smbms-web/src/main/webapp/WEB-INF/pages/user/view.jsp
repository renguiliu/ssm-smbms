<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<html>
<head lang="en">
    <base href="<%=basePath%>">
    <meta charset="UTF-8">
    <title>超市账单管理系统</title>
    <link rel="stylesheet" href="css/public.css"/>
    <link rel="stylesheet" href="css/style.css"/>
    <link rel="stylesheet" href="bootstrap/css/bootstrap.css">
    <style>
        p{
            width: 450px;
            margin: 0 auto;
        }
    </style>
</head>
<body>
<!--头部-->
<jsp:include page="../common/header.jsp"/>
<!--主体内容-->
<section class="publicMian">
    <jsp:include page="../common/left.jsp"/>
    <div class="right">
        <div class="location">
            <strong>你现在所在的位置是:</strong>
            <span>用户管理页面 >> 用户信息查看页面</span>
        </div>
        <div class="providerView">
            <p style="text-align: center;"><img src="${user.picPath}" style="width: 150px;height: 150px;border-radius: 50%"/></p>
            <p><strong>用户编号：</strong><span>${requestScope.user.userCode}</span></p>
            <p><strong>用户名称：</strong><span>${requestScope.user.userName}</span></p>
            <p>
                <strong>用户性别：</strong>
                <span>
                    <c:if test="${requestScope.user.gender==1}">男</c:if>
                     <c:if test="${requestScope.user.gender==2}">女</c:if>
                </span>
            </p>
            <p><strong>出生日期：</strong>
                <span><fmt:formatDate value="${requestScope.user.birthday}" pattern="yyyy年MM月dd日"/></span>
            </p>
            <p><strong>用户电话：</strong><span>${requestScope.user.phone}</span></p>
            <p><strong>用户地址：</strong><span>${requestScope.user.address}</span></p>
            <p><strong>用户类别：</strong><span>${requestScope.user.role.roleName}</span></p>
            <a href="javascript:history.back()">返回</a>
        </div>
    </div>
</section>
<footer class="footer">
    版权归北大青鸟
</footer>
<script src="js/time.js"></script>

</body>
</html>