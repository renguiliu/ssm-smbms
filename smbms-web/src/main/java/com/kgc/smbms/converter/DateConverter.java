package com.kgc.smbms.converter;

import org.springframework.core.convert.converter.Converter;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 日期转换器
 *
 * @author lrg on 2021/7/6
 * @since 1.0.0
 */
public class DateConverter implements Converter<String, Date> {

    /**
     * yyyy-MM-dd
     * yyyy-MM-dd hh:mm:ss
     * yyyy/MM/dd
     */
    private String[] patterns;

    /**
     * 2021/03/04
     * @param dateStr
     * @return
     */
    @Override
    public Date convert(String dateStr) {
        Date date = null;
        SimpleDateFormat format = null;
        for (String pattern : patterns) {
            format = new SimpleDateFormat(pattern);
            try {
                date = format.parse(dateStr);
                break;
            } catch (ParseException e) {
                continue;
            }
        }
        return date;
    }

    public void setPatterns(String[] patterns) {
        this.patterns = patterns;
    }
}
