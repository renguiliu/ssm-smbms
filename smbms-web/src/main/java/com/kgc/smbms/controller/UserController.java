package com.kgc.smbms.controller;

import com.alibaba.fastjson.JSON;
import com.github.pagehelper.PageInfo;
import com.kgc.smbms.entity.Role;
import com.kgc.smbms.entity.User;
import com.kgc.smbms.service.RoleService;
import com.kgc.smbms.service.UserService;
import com.kgc.smbms.utils.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 用户管理模块的控制器
 *
 * @author lrg on 2021/6/28
 * @since 1.0.0
 */
@Controller
@RequestMapping("/user")
public class UserController extends BaseController {

    @Autowired
    private UserService userService;
    @Autowired
    private RoleService roleService;


    @RequestMapping(value = "/validata",method = RequestMethod.POST)
    @ResponseBody
    public Object validataData(String validataParam,
                               Integer validataType){
        Map<String,Object> result = new HashMap<>();
        if(validataType==null || "".equals(validataType)
                || validataParam==null || "".equals(validataParam)){
            result.put("code",500);
            result.put("message","参数不能为空");
            result.put("data","");
            //将返回结果转成json字符串
            String jsonString = JSON.toJSONString(result);
            return jsonString;
        }
        Map<String,Object> params = new HashMap<>();
        params.put("validataParam",validataParam);
        params.put("validataType",validataType);
        //调用业务逻辑层查询数据
        boolean flag = userService.validataData(params);
        //创建一个map集合来存结果
        result.put("code",200);
        result.put("message","请求成功");
        result.put("data",flag);
        //将返回结果转成json字符串
        String jsonString = JSON.toJSONString(result);
        return jsonString;
    }

    /*@RequestMapping(value = "/detail",method = RequestMethod.POST,produces = {"application/json;charset=UTF-8"})*/
    @RequestMapping(value = "/detail",method = RequestMethod.POST)
    @ResponseBody
   /* public Object detail(Integer id){*/
    /*public User detail(Integer id){*/
    /*public List<User> detail(Integer id){*/
    public Map<String,Object> detail(Integer id){
        Map<String,Object> result = new HashMap<>();
        if(null==id || id==0){
            result.put("code",500);
            result.put("message","参数不能为空");
            result.put("data","");
            //将返回结果转成json字符串
            /*String jsonString = JSON.toJSONString(result);*/
            return result;
        }
        //调用业务员逻辑层去执行查询
        User user = userService.getUserById(id);
        result.put("code",200);
        result.put("message","请求成功");
        result.put("data",user);
        //将返回结果转成json字符串
        /*String jsonString = JSON.toJSONString(result);*/
        return result;
    }

    @RequestMapping(value="/view/{id}",method = RequestMethod.GET)
    public String detail(@PathVariable Integer id,
                         RedirectAttributes ra,
                         Model model){
        if(id==null){
            ra.addFlashAttribute("message","用户的ID不能为空！");
            return "redirect:/user/index";
        }
        //如果代码能走到这里说明ID不为空
        User user = userService.getUserById(id);
        model.addAttribute("user",user);
        return "user/view";
    }

    /**
     * 显示修改页面
     * @param id
     * @param ra
     * @param model
     * @return
     */
    @RequestMapping(value="/updateview/{id}",method = RequestMethod.GET)
    public String updateView(@PathVariable Integer id,
                             RedirectAttributes ra,
                             Model model){
        if(id==null){
            ra.addFlashAttribute("message","修改用户的ID不能为空！");
            return "redirect:/user/index";
        }
        //如果代码能走到这里说明ID不为空
        User user = userService.getUserById(id);
        List<Role> roleList = roleService.findAll();
        model.addAttribute("user",user);
        model.addAttribute("rolelist",roleList);
        return "user/update";
    }

    /**
     * 修改用户信息的方法
     * @param user
     * @param session
     * @param ra
     * @return
     */
    @RequestMapping(value="/update",method=RequestMethod.POST)
    public String update(User user,
                         HttpSession session,
                         RedirectAttributes ra){
        //获取当前登录的用户对象
        User loginUser = (User) session.getAttribute(Constants.LOGIN_USER);
        user.setModifyBy(loginUser.getId());
        //调用业务逻辑层去执行添加的操作
        boolean flag = userService.update(user);
        String message = "修改失败!";
        if(flag){//表示修改成功
            message = "修改成功";
        }
        ra.addFlashAttribute("message",message);
        return "redirect:/user/index";
    }

    /**
     * 显示添加页面
     * @param user
     * @param model
     * @return
     */
    @RequestMapping(value="/addview",method= RequestMethod.GET)
    public String addView(@ModelAttribute("user")User user,
                          Model model){
        //查询角色列表
        List<Role> roleList = roleService.findAll();
        model.addAttribute("roleList",roleList);
        return "user/add";
    }

    /**
     * 添加用户数据
     * @param user
     * @return
     */
    @RequestMapping(value = "/add",method=RequestMethod.POST)
    public String add(RedirectAttributes ra, @Valid User user, BindingResult bindingResult,Model model){
        if(bindingResult.hasErrors()){//如果bindingResult对象中有错误信息
            List<FieldError> fieldErrors = bindingResult.getFieldErrors();
            for (FieldError fieldError : fieldErrors) {
                String field = fieldError.getField();//获取错误的字段
                String message = fieldError.getDefaultMessage();//获取错误的信息
                System.out.println(field+":------>"+message);
                model.addAttribute(field,message);
            }
            return "user/add";
        }

        boolean flag = userService.create(user);
        String message = "添加失败";
        if(flag){//添加成功
            message = "添加成功";
        }
        ra.addFlashAttribute("message",message);
        //如果代码能走到这里说明，添加失败 //跳转到显示页面
        return "redirect:/user/index";//重定向到控制器
    }

    /**
     * 查询用户首页数据
     * @return
     */
    @RequestMapping(value="/index")
    public String list(String username,
                       Integer role,
                       Integer currentIndex,
                       Integer size,
                       Model model){
        currentIndex = currentIndex==null
                ? Constants.PageUtils.currentIndex : currentIndex;
        size = size == null
                ? Constants.PageUtils.pageSize : size;
        //将查询参数存储在request中们用于页面回显
        model.addAttribute("username",username);
        model.addAttribute("userRole",role);
        //调用业务逻辑层去查询数据
        PageInfo<User> page = userService.getUserByCondition(currentIndex, size,
                username, role);
        //将数据存储到model中
        model.addAttribute("page",page);
        //查询角色
        List<Role> roleList = roleService.findAll();
        //将数据存储到request中
        model.addAttribute("roleList",roleList);
        //跳转到用户管理的首页中去 /WEB-INF/pages/user/list.jsp
        return "user/list"; //转发
    }


}
