package com.kgc.smbms.controller;

import com.alibaba.druid.support.json.JSONUtils;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.apache.commons.io.FileUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;

/**
 * 文件上传的控制器
 *
 * @author lrg on 2021/6/30
 * @since 1.0.0
 */
@Controller
public class UploadController {

    @RequestMapping(value = "/manyupload",produces = {"application/json;charset=UTF-8"})
    @ResponseBody
    public Object manyFileUpload(@RequestParam MultipartFile[] headerPic,
                                 HttpServletRequest request) throws IOException {
        List<Map<String,Object>> finalResult = new ArrayList<>();
        for (MultipartFile multipartFile : headerPic) {
            Map<String, Object> uploadResult = upload(multipartFile, request);
            finalResult.add(uploadResult);
        }
        return JSON.toJSONString(finalResult);
    }
    /**
     * 文件上传的方法
     * @param headerPic
     * @param request
     * @return
     * @throws IOException
     */
    private Map<String,Object> upload(MultipartFile headerPic,
                             HttpServletRequest request) throws IOException {
        Map<String,Object> result = new HashMap<>();
        //如果上传文件为空
        if(headerPic.isEmpty()){
            result.put("code",500);
            result.put("message","");
            result.put("data","");
            return result;
        }
        //如果代码能走到这里说明，上传文件不为空
        String path = "";//图片存储的路径
        //定义允许上传的文件的后缀
        String allowFileSuffix = "jpg,png,bmp";
        String[] allowFileSuffixArr = allowFileSuffix.split(",");//将上面的字符串以,分割
        //获取上传文件的名称
        String filename = headerPic.getOriginalFilename();
        //获取上传文件的后缀
        String fileHz = filename.substring(filename.lastIndexOf(".")+1);
        //判断文件后缀
        List<String> allowFileSuffixList = Arrays.asList(allowFileSuffixArr);
        if(!allowFileSuffixList.contains(fileHz)){
            result.put("code",500);
            result.put("message","上传的文件格式不正确，只能是"+allowFileSuffix);
            result.put("data","");
            return result;
        }
        //定义允许上传的文件的大小
        long allowFileSize = 200*1024;
        //获取上传文件的大小
        long uploadFileSize = headerPic.getSize();
        if(uploadFileSize>allowFileSize){//不合法
            result.put("code",500);
            result.put("message","上传的文件的过大，只能是"+200+"kb");
            result.put("data","");
            return result;
        }
        //获取上传文件的输入流对象
        InputStream is = headerPic.getInputStream();
        //准备第二个参数
        //获取服务器的地址
        String serverPath = request.getSession().getServletContext().getRealPath("/upload");
        System.out.println("serverPath:"+serverPath);

        //给上传的文件名称重新命名
        String newFileName = System.currentTimeMillis()+filename;

        String uploadFilePath = serverPath+File.separator+newFileName;
        System.out.println("uploadFilePath:"+uploadFilePath);
        File file = new File(uploadFilePath);
        //上传文件
        FileUtils.copyInputStreamToFile(is,file);
        path = "upload"+File.separator+newFileName;
        result.put("code",200);
        result.put("message","文件上传成功");
        result.put("data",path);
        return result;
    }

    /*
     * springmvc返回接送数据
     * 1.添加fastjson依赖
     * 2.在方法上面添加@ResponseBody,作用就是将方法的返回值直接写入http response体重
     */
    @RequestMapping(value = "/upload",produces = {"application/json;charset=UTF-8"})
    @ResponseBody
    public Object fileUpload(MultipartFile headerPic,
                             HttpServletRequest request) throws IOException {
        List<Map<String,Object>> finalResult = new ArrayList<>();
        Map<String, Object> uploadResult = upload(headerPic, request);
        finalResult.add(uploadResult);
        return JSON.toJSONString(finalResult);
    }


    /**
     * 删除服务器上的图片：前端传递过来的值为:upload/xxx.jpg
     * @param imgPath
     * @param request
     * @return
     * @throws IOException
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(String imgPath,
                         HttpServletRequest request) throws IOException {
        Map<String,Object> result = new HashMap<>();
        //获取upload文件夹在服务器上的路径
        String serverPath = request.getSession().getServletContext().getRealPath("/upload");
        //截取文件名称
        String filename = imgPath.substring(imgPath.indexOf(File.separator) + 1);
        //拼接文件在服务器上的路径
        String uploadFilePath = serverPath+File.separator+filename;
        //创建要删除的文件对象
        File file = new File(uploadFilePath);
        boolean flag = false;
        if(file.exists()){
            flag = file.delete();
        }
        result.put("flag",flag);
        return JSON.toJSONString(result);
    }
}
