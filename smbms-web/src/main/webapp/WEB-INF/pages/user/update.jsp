<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<html>
<head lang="en">
    <base href="<%=basePath%>">
    <meta charset="UTF-8">
    <title>超市账单管理系统</title>
    <link rel="stylesheet" href="css/public.css"/>
    <link rel="stylesheet" href="css/style.css"/>
    <link rel="stylesheet" href="bootstrap/css/bootstrap.css">
</head>
<body>
<!--头部-->
<jsp:include page="../common/header.jsp"/>
<!--主体内容-->
<section class="publicMian">
    <jsp:include page="../common/left.jsp"/>
    <div class="right">
        <div class="location">
            <strong>你现在所在的位置是:</strong>
            <span>用户管理页面 >> 用户添加页面</span>
        </div>
        <div class="providerAdd">
            <form action="user/update" method="post" id="updateForm">
                <!--div的class 为error是验证错误，ok是验证成功-->
                <div>
                    <label for="userName">用户名称：</label>
                    <input type="hidden" name="id" value="${requestScope.user.id}"/>
                    <input type="text" name="userName" id="userName" value="${requestScope.user.userName}"/>
                    <span >*请输入用户名称</span>
                </div>
                <div>
                    <label >用户性别：</label>

                    <select name="gender">
                        <option value="1" <c:if test="${requestScope.user.gender==1}">selected</c:if>>男</option>
                        <option value="2" <c:if test="${requestScope.user.gender==2}">selected</c:if>>女</option>
                    </select>
                    <span></span>
                </div>
                <div>
                    <label for="data">出生日期：</label>
                    <%--<input type="text" name="birthday" class="text Wdate"
                           onclick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss'});" id="data"
                           value="<fmt:formatDate value='${requestScope.user.birthday}' pattern='yyyy-MM-dd hh:mm:ss'/>"/>--%>
                    <input type="text" name="birthday" class="text Wdate"
                           onclick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss'});" id="data"
                           value="<fmt:formatDate value='${requestScope.user.birthday}' pattern='yyyy-MM-dd hh:mm:ss'/>"/>
                    <span >*</span>
                </div>
                <div>
                    <label for="userphone">用户电话：</label>
                    <input type="text" name="phone" id="userphone" value="${requestScope.user.phone}"/>
                    <span >*</span>
                </div>
                <div>
                    <label for="userAddress">用户地址：</label>
                    <input type="text" name="address" id="userAddress" value="${requestScope.user.address}"/>
                </div>
                <div id="role">
                    <label for="userAddress">用户角色：</label>
                    <c:forEach items="${requestScope.rolelist}" var="role">
                        <input type="radio" name="userRole" value="${role.id}" <c:if test="${role.id==user.userRole}">checked</c:if>/>${role.roleName}
                    </c:forEach>
                </div>
                <div class="providerAddBtn">
                    <!--<a href="#">保存</a>-->
                    <!--<a href="userList.html">返回</a>-->
                    <input type="button" value="保存" id="updateBtn"/>
                    <input type="button" value="返回" onclick="history.back(-1)"/>
                </div>
            </form>
        </div>

    </div>
</section>
<footer class="footer">
    版权归北大青鸟
</footer>
<script src="js/time.js"></script>
<script type="text/javascript" src="js/jquery-1.12.4.js"></script>
<script type="text/javascript" src="My97DatePicker/WdatePicker.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $("#updateBtn").on('click',function(){
            $("#updateForm").submit();
        });
    });
</script>
</body>
</html>