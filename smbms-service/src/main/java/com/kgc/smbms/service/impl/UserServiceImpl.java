package com.kgc.smbms.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.kgc.smbms.entity.User;
import com.kgc.smbms.mapper.UserMapper;
import com.kgc.smbms.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author lrg on 2021/6/10
 * @since 1.0.0
 */
@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserMapper userMapper;

    @Override
    public PageInfo<User> getUserByCondition(Integer currentIndex, Integer pageSize, String username, Integer userRole) {

        PageInfo<User> page;
        //将当前页码和每页显示多少条数据设置在PageHelper对象中
        PageHelper.startPage(currentIndex,pageSize,"u.creationDate desc");

        Map<String,Object> params = new HashMap<>();
        params.put("username",username);
        params.put("userRole",userRole);
        //之前怎么查询就怎么查询不用管分页
        List<User> userList = userMapper.getUserByCondition(params);

        //PageInfo是mybatis分页插件中提供的
        page = new PageInfo<>(userList);
        return page;
    }

    @Override
    public boolean validataData(Map<String, Object> params) {
        Integer count = userMapper.validateData(params);
        if(count>0){//说明数据库中有该数据存在
            return false;//表示该用户名不可用
        }
        return true;//表示该用户名可用
    }

    @Override
    public boolean create(User user) {
        //user.setCreatedBy(1);//设置创建者，获取当前登录对象
        user.setCreationDate(new Date());//设置创建事件
        Integer num = userMapper.create(user);
        if(num>0){
            return true;
        }
        return false;
    }

    @Override
    public User login(String loginParams, String password) {
        User user;
        List<User> userList = userMapper.login(loginParams);
        if(userList==null || userList.size()!=1){
            return null;
        }
        user = userList.get(0);
        if(!user.getUserPassword().equals(password)){
            return null;
        }
        return user;
    }

    @Override
    public boolean delete(int id) {
        Integer num = userMapper.deleteByPrimaryKey(id);
        int i = 1/0;
        if(num>0){
            return true;
        }
        return false;
    }

    @Override
    public User getUserById(int id) {
        User user = null;
        user = userMapper.getUserByPrimaryKey(id);
        return user;
    }

    @Override
    public boolean update(User user) {
        user.setModifyDate(new Date());//设置创建事件
        Integer num = userMapper.updateByPrimaryKey(user);
        if(num>0){
            return true;
        }
        return false;
    }
}
